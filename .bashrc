#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

neofetch

## ls
alias ls='ls --color=auto'
alias ll='ls -lah --color=auto'
alias la='ls -A --color=auto'
alias l.='ls -d .* --color=auto'

# kubectl
alias kubens='kubectl config set-context --current --namespace '
alias kubectx='kubectl config use-context '

# pacman
alias removeorphans='sudo pacman -Qdtq | sudo pacman -Rns -'

# fast cd
alias ..='cd ..'
alias ...='cd ../../../'
alias ....='cd ../../../../'
alias .....='cd ../../../../'
alias .4='cd ../../../../'
alias .5='cd ../../../../..'

# useful keys
alias bc='bc -l'
alias mkdir='mkdir -pv'
alias grep='grep --color=auto'
alias mount='mount |column -t'
alias path='echo -e ${PATH//:/\\n}'
alias ping='ping -c 5'
alias diff='colordiff'  # colordiff package
alias chown='chown --preserve-root'
alias chmod='chmod --preserve-root'
alias chgrp='chgrp --preserve-root'
alias rm='rm -I --preserve-root'
alias mv='mv -i'
alias cp='cp -i'
alias ln='ln -i'
alias root='sudo -i'
alias free='free -h'
alias df='df -h'
alias wget='wget -c'
alias untar='tar -xvf'
alias ports='netstat -tulnp'

PS1='\[\e]0;\u@\h: \w\a\]\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '

source <(kubectl completion bash)
